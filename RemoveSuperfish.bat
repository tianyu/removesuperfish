@ECHO OFF

echo This script requires administrative permissions. Attempting to elevate now...

%= If you wish to use NSS (set -USENSS) to untrust the Superfish root CA in Firefox =%
%= uncomment the following line by removing the two colons at the front =%
::SET ENABLENSS=1

IF DEFINED ENABLENSS (PowerShell -NoProfile -ExecutionPolicy Bypass -Command "& {Start-Process PowerShell -ArgumentList '-NoProfile -ExecutionPolicy Bypass -File """"%~dp0rmsf.ps1"""" -USENSS """"1"""" ' -Verb RunAs}";) ELSE (PowerShell -NoProfile -ExecutionPolicy Bypass -Command "& {Start-Process PowerShell -ArgumentList '-NoProfile -ExecutionPolicy Bypass -File """"%~dp0rmsf.ps1"""" -USENSS """"0"""" ' -Verb RunAs}";)