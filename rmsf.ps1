﻿# This is a PowerShell script. If you are reading this, it could mean that
# you do not have it installed. PowerShell is included in Windows XP SP2 or
# above. For your safety, please consider upgrading your system and keep it
# up to date. Thank you. ~tianyu

<#
DESCRIPTION
The purpose of this script is to remove Superfish VisualDiscovery found on
Lenovo-branded PCs. Lenovo started to pre-install this malware as early as 
2010. This malware can make the host computer vulnerable to Man-in-the-Middle
attack, and simply uninstalling it will not fix the vulnerbility. This script
will attempt to:
    - Check PowerShell version. If PowerShell version is 1 (came with Windows
        XP SP2 and Windows Vista), prompt and exit as many functions plainly
        will not work. If PowerShell version is 2 or 3, prompt user that some
        functions may not work, but continue executing the script.
    - Uninstall Superfish VisualDiscovery, if it is not already removed.
    - Clear temporary files + clean up browsers, Include:
        > %LOCALAPPDATA%\Temp\*
        > Internet Explorer: Cache, Cookies
        > Google Chrome: Cache, Cookies
        > Firefox: Cache, Cookies
    - Move the Superfish root CA to "Untrusted Certificates" store in Windows 
        Certificate Manager, if it is not already removed. This will not 
        affect Firefox, as it maintains its own certificate database.
    - Force Windows to check Microsoft's Certificate Trust List by removing 
        its cache.
    - If Firefox is installed and the certificate is found in its Certificate 
        Database. There are two ways to do this:
            > Reset Firefox certificate database by removing it (default). A 
            new database will be initialised next time when Firefox is opened.
            According to Mozilla (https://wiki.mozilla.org/CA:UserCertDB), 
            "Deleting a root certificate that is in the default root store is 
            equivalent to turning off all of the trust bits for that root. 
            Therefore, even though the root certificate will re-appear in the 
            Certificate Manager, it will be treated as though you changed the 
            trust bits of that root certificate to turn them all off." However
            I could not replicate this all the time. 
            > Use certutil in Mozilla's Network Security Services (NSS). This
            is different from the certuitl provided by Microsoft and can modify
            Firefox's certification database (cert8.db). With that, I can set 
            the Superfish root CA to be prohibited. Problem? Mozilla does not
            provide binary for NSS, so I have to build it, which means the
            utility is not signed. Also, it requires installing VC++2013 Runtime
            (x86, and if applicable, x64). This is why it is not enabled by 
            default. To enable this option, modify the batch file that 
            initilises this script so that ``-USENSS true'' is its runtime 
            argument. 
      So I settled with resetting certificate database to default. According to
        Mozilla (https://wiki.mozilla.org/CA:UserCertDB), "Deleting a root 
        certificate that is in the default root store is equivalent to turning 
        off all of the trust bits for that root. Therefore, even though the root 
        certificate will re-appear in the Certificate Manager, it will be treated 
        as though you changed the trust bits of that root certificate to turn 
        them all off."
    - Reset Winsock.
    - Prompt to restart.

LICENSE
    This script is released under GPLv3.
    If -USENSS flag is true, files under the ``nss'' folder are licensed through 
    Mozilla Public License. ``vcredist_x64.exe'' and ``vcredist_x86.exe'' are
    licensed with MICROSOFT SOFTWARE LICENSE TERMS - MICROSOFT VISUAL C++ 
    REDISTRIBUTABLE FOR VISUAL STUDIO 2013. 
    You accept the aforementioned terms and conditions by using this script. If
    you do not accept MICROSOFT SOFTWARE LICENSE TERMS - MICROSOFT VISUAL C++ 
    REDISTRIBUTABLE FOR VISUAL STUDIO 2013, please make sure that -USENSS flag is
    set as false (-USENSS is false by default).

TESTED ON WINDOWS 8.1
~tianyu
#>
param([int]$USENSS=0)

$VDDIR = 'C:\Program Files (x86)\Lenovo\VisualDiscovery'
$VDUNINSTALLER = 'C:\Program Files (x86)\Lenovo\VisualDiscovery\uninstall.exe'
$PWD = split-path -parent $MyInvocation.MyCommand.Definition
# WINVER[0] - Major Version
# WINVER[1] - Minor Version
# WINVER[2] - Build
$WINVER = ((Get-CimInstance Win32_OperatingSystem).Version).split('.')
$PSMAJORVER = $Major = ($PSVersionTable).PSVersion.Major

If ($WINVER[0] -lt 6) {
Write-Output "It appears that you are still using Windows XP. After 12 years of service, Microsoft ended Windows XP extended support on 8 April, 2014, which means you won't be able to receive any security updates from them - and that should be something more important to this Superfish VisualDiscovery vulnerbility. Please considering upgrading to a modern operating system. Plus, many functions used in this PowerShell script will not work for PowerShell 1, which comes with Windows XP Service Pack 2." 
}

# Begin script execution
If ($USENSS -eq 1) {
    Write-Output "This script will help removing Superfish VisualDiscovery and set the root CA installed with it to be untrusted. By using this script, you agree to MICROSOFT SOFTWARE LICENSE TERMS - MICROSOFT VISUAL C++ REDISTRIBUTABLE FOR VISUAL STUDIO 2013."
} Else {
    Write-Output "This script will help removing Superfish VisualDiscovery and set the root CA installed with it to be untrusted."
}

$ErrorActionPreference="SilentlyContinue"
Stop-Transcript | out-null
$ErrorActionPreference = "Continue"
Start-Transcript $PWD\superfishremoval.log
$timer = [System.Diagnostics.Stopwatch]::StartNew()

Write-Output "Current directory: $PWD"

# See if VisualDiscovery is still installed
$isVDInstalled = Test-Path $VDDIR
$isChromeCachePresent = Test-Path "$env:LOCALAPPDATA\Google\Chrome\User Data\Default\Cache"
$isChromeCookiesPresent = Test-Path "$env:LOCALAPPDATA\Google\Chrome\User Data\Default\Cookies"
$isChromeCookiesJournalPresent = Test-Path "$env:LOCALAPPDATA\Google\Chrome\User Data\Default\Cookies-journal"
$isFirefoxCachePresent = Test-Path "$env:APPDATA\Mozilla\Firefox\Profiles\*.default\cache2"
$isFirefoxCookiesPresent = Test-Path "$env:APPDATA\Mozilla\Firefox\Profiles\*.default\cookies.sqlite"
$isSuperfishRootCATrusted = Test-Path Cert:\CurrentUser\Root\C864484869D41D2B0D32319C5A62F9315AAF2CBD;
$isFirefoxInstalled = (Test-Path "${env:ProgramFiles(x86)}\Mozilla Firefox") -or (Test-Path "$env:ProgramFiles\Mozilla Firefox")
$isFirefoxCertDBPresent = Test-Path "$env:APPDATA\Mozilla\Firefox\Profiles\*.default\cert8.db"

If ($PSMAJORVER -eq 1) {
    Write-Output "It appears that this script is being executed in PowerShell 1. Many functions used in this script will not work in this environment. Please consider upgrading your operating system. This script will exit in 5 seconds."
    Start-Sleep -s 5
    exit
} Elseif (($PSMAJORVER -eq 2) -or ($PSMAJORVER -eq 3)) {
    Write-Output "It appears that this script is being executed in PowerShell $PSMAJORVER. Please be aware that some functions might not work in this environment."
}

If ($isVDInstalled) {
    Write-Output "[$($timer.Elapsed.ToString())]Found VisualDiscovery installation. Attempting to uninstall."
    Start-Process $VDUNINSTALLER /S -NoNewWindow -Wait
    Remove-Item $VDDIR
    Write-Output "[$($timer.Elapsed.ToString())]VisualDiscovery uninstalled."
    
}

Write-Output "[$($timer.Elapsed.ToString())]Attempting to remove temporary files."
Remove-Item $env:LOCALAPPDATA\Temp\* -recurse
Write-Output "[$($timer.Elapsed.ToString())]Attempting to remove cache and cookies from Internet Explorer."
&{RunDll32.exe InetCpl.cpl, ClearMyTracksByProcess 8}
&{RunDll32.exe InetCpl.cpl, ClearMyTracksByProcess 2}

If ($isChromeCachePresent -or $isChromeCookiesPresent -or $isChromeCookiesJournalPresent) {
    Write-Output "[$($timer.Elapsed.ToString())]Attempting to remove cache and cookies from Google Chrome."
    If ($isChromeCachePresent) {
        Remove-Item "$env:LOCALAPPDATA\Google\Chrome\User Data\Default\Cache\*" -recurse
    }
    If ($isChromeCookiesPresent) {
        Remove-Item "$env:LOCALAPPDATA\Google\Chrome\User Data\Default\Cookies"
    }
    If ($isChromeCookiesJournalPresent) {
        Remove-Item "$env:LOCALAPPDATA\Google\Chrome\User Data\Default\Cookies-journal"
    }
}

If ($isFirefoxCachePresent -or $isFirefoxCookiesPresent) {
    Write-Output "[$($timer.Elapsed.ToString())]Attempting to remove cache and cookies from Mozilla Firefox."
    If ($isFirefoxCachePresent) {
        Remove-Item "$env:APPDATA\Mozilla\Firefox\Profiles\*.default\cache2\*" -recurse
    }
    If ($isFirefoxCookiesPresent) {
        Remove-Item "$env:APPDATA\Mozilla\Firefox\Profiles\*.default\cookies.sqlite"
    }
}

if ($isSuperfishRootCATrusted) {
    Write-Output "[$($timer.Elapsed.ToString())]Attempting to untrust Superfish root CA."
    Move-Item -Path Cert:\CurrentUser\Root\C864484869D41D2B0D32319C5A62F9315AAF2CBD -Destination Cert:\CurrentUser\Disallowed\
}

Write-Output "[$($timer.Elapsed.ToString())]Attempting to delete Certificate Trust List cache and retrieve update from Microsoft."
&{certutil.exe -urlcache * delete}

If ($isFirefoxInstalled -and $isFirefoxCertDBPresent -and ($USENSS -eq 0)) {
    Write-Output "[$($timer.Elapsed.ToString())]Attempting to untrust Superfish root CA for Firefox by resetting its certification database."
    $firefoxCertDB = Resolve-Path "$env:APPDATA\Mozilla\Firefox\Profiles\*.default\cert8.db"
    Remove-Item $firefoxCertDB
} Elseif ($isFirefoxInstalled -and $isFirefoxCertDBPresent -and ($USENSS -eq 1)) {
    Write-Output "[$($timer.Elapsed.ToString())]Attempting to Install Visual C++ Redistributable x86."
    Start-Process $PWD\vcredist_x86.exe -ArgumentList "/install /quiet /norestart" -NoNewWindow -Wait
    If ([Environment]::Is64BitProcess) {
        Write-Output "[$($timer.Elapsed.ToString())]Attempting to Install Visual C++ Redistributable x64."
        Start-Process $PWD\vcredist_x64.exe -ArgumentList "/install /quiet /norestart" -NoNewWindow -Wait
    }
    Write-Output "[$($timer.Elapsed.ToString())]Attempting to untrust Superfish root CA for Firefox using Mozilla certutil."
    $firefoxCertDBPath = Resolve-Path "$env:APPDATA\Mozilla\Firefox\Profiles\*.default\"
    $certutilNSSPath = "$PWD\nss\certutil.exe"
    Start-Process "$certutilNSSPath" -ArgumentList "-M -n ""Superfish, Inc."" -t """" -d ""$firefoxCertDBPath""" -NoNewWindow -Wait
    Start-Process "$certutilNSSPath" -ArgumentList "-D -n ""Superfish, Inc."" -d ""$firefoxCertDBPath""" -NoNewWindow -Wait
}

Write-Output "[$($timer.Elapsed.ToString())]Attempting to reset Winsock."
&{netsh winsock reset}

Write-Output "[$($timer.Elapsed.ToString())]Tasks completed."
Write-Output "[$($timer.Elapsed.ToString())]Restart in 60 seconds. To cancel, please close this window."

Start-sleep -s 60
Restart-Computer

Stop-Transcript